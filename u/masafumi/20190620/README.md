Masafumi pointed out that he can't get at hdf5 attributes using Autoplot.  This 
is to explore this new feature.

Note: 
~~~~~
/home/jbf
nudnik$ h5dump /opt/project/juno/web/mwg/hdf5/JunoFoot_PJ19_20190617_all.hdf5 > foo.txt
~~~~~
shows all the data and attributes within a HDF5 file.

See the bug ticket: https://sourceforge.net/p/autoplot/bugs/1960/